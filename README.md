Name: ABHIMANYU C V
Roll No: 03
Name and Number of the Experiment: Familiarization of LINUX OS EXP 1
Date:03-01-18
1. Command: ls
Purpose: List the files and fields on the current directory.
Syntax:ls
Example:
2. Command: pwd
Purpose:Adress of current Directory
Syntax:pwd
Example:
3. Command: cd
Purpose:Change directory
Syntax:cd <name of directory>
Example:
4. Command: clear
Purpose:Set the terminal screen to latest line
Syntax:clear
Example:5. Command: man
Purpose:Interface to the on-line reference manuals
Syntax:man <keyword>
Example:
6. Command: cat
Purpose:Interface to the on-line reference manuals
Syntax:cat <file>
Example:
7. Command: grep
Purpose:print lines matching a pattern
Syntax:grep
Example:
8. Command: mkdir
Purpose:make new directory
Syntax:mkdir <Name of directory>
Example:9. Command: mv
Purpose:move (rename) files
Syntax:mv <directory> <destination>
Example:
10. Command: cp
Purpose:Copy files
Syntax:cp <directory> <destination>
Example:
11. Command: rm
Purpose:remove files or directories
Syntax:rm <file/dir name>
Example:
12. Command: nano
Purpose:editorSyntax:nano <filename>
Example:
13. Command: chmod
Purpose:change file mode bits
Syntax:chmod <mode> <file>
Example:
14. Command: su
Purpose:change user ID or become superuser
Syntax:su <Username>
Example:
15. Command: sudo
Purpose:execute a command as another user
Syntax:sudo <key>
Example:16. Command: uname
Purpose:print system information
Syntax:uname
Example:
17. Command: who
Purpose:show who is logged on
Syntax:who
Example:
18. Command: users
Purpose:print the user names of users currently logged in to the current host
Syntax:users
Example:
19. Command: top
Purpose:display Linux processes
Syntax:top
Example:20. Command: ps
Purpose:report a snapshot of the current processes.
Syntax:ps
Example:
21. Command: kill
Purpose:kill a process
Syntax:kill <pid>
Example:
22. Command: killall
Purpose:kill a process
Syntax:killall <process name>
Example: